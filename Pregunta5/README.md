# Pregunta 5
```c++
// vector de posiciones del jugador
player->pos(x,y,z);

// vector de direccion del jugador (direccion donde el jugador este mirando actualmente)
player->pForward(x,y,z);

// lista de enemigos activa
vector<*Enemigo> activeEnemies;

// debe existir una clase Player que sea un UActor
class Player{}

Enemigo Player::LockOnEnemie(vector<*Enemigo> &_activeEnemies){
    // este valor vendria a ser la posicion del jugador que se nombro al inicio player->pos(x,y,z)
    FVector pos = GetOwner()->GetActorLocation();

    // valor que ayudara a almacenar el enemigo a retornar, al iniciar sera nulo por si no hay enemigos que utilizar en el lockon
    Enemigo enemigoRespuesta = null;

    // Valor de distancia inicial
    float distanciaMenor = 9000000.f

    for (auto enemie:_activeEnemies){
        // evitamos verificar a los enemigos que no hayan sido renderizados hace un segundo
        if(enemie->WasRecentlyRendered(1.f)){
            // esta funcion dara la distancia entre la posicion del jugador y la posicion del enemigo actualmente revisado
            float distancia = FVector::Dist(pos, enemie->GetActorLocation());
            // validamos que no haya mas de 20u de distancia entre personaje y enemigo
            if (distancia <= 20.f){
                if (distancia < distanciaMenor){
                    float angle = (enemie->GetActorLocation() - player->GetActorLocation()).ToOrientationRotator().Yaw - player->GetPFoward().ToOrientationRotator().Yaw;
                    if (angle < x && angle > -x){
                        distanciaMenor = distancia;
                        enemigoRespuesta = enemie;
                    }
                }
            }
        }
    }
    return enemigoRespuesta;
}
```
Asumiendo que hay funciones para tener el vector de pFoward llamada GetPFoward, se puede recorrer el arreglo de enemigos activos, verificando que el enemigo se haya renderizado en el ultimo segundo, para evitar asi entrar a revisar enemigos que no estan siendo renderizados. Luego de eso se debe verificar si la distancia es mayor a las 20u establecidas, para ello se utilizo la funcion Dist de FVector, esta puede ser cambiada por FVector::Dist2D si se cree que la coordenada z es inecesaria para este calculo de la unidad. Para no escoger simplemente al primer enemigo se utiliza un valor de distancia menor, la cual ayudara a elegir el enemigo mas cercano que se encuentre dentro de los angulos establecidos de preferencia llamado x. para ello se utiliza en yaw de la orientacion del actor para saber a que angulo se encutran de la vision del personaje.

[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)