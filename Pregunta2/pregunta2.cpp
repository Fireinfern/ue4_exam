#include <iostream>
#include <math.h>
#include <stdio.h>
#include <vector>

class Rectangulo
{
private:
    float x;
    float y;
    float w;
    float h;

public:
    Rectangulo() {}
    Rectangulo(float _x, float _y, float _w, float _h)
    {
        x = _x;
        y = _y;
        w = _w;
        h = _h;
    }
    ~Rectangulo() {}
    float GetX() { return x; }
    float GetY() { return y; }
    float GetW() { return w; }
    float GetH() { return h; }
    bool CollisionEntreTodosLosTriangulos(std::vector<Rectangulo *> &rectangulos)
    {
        bool ThisRect = false;
        for (auto rect : rectangulos)
        {
            if (!ThisRect && rect->GetX() == x && rect->GetY() == y)
                ThisRect = true;
            else
            {
                if (!((rect->GetX() + (rect->GetW() / 2)) >= (x - (w / 2)) &&
                    (x + (w / 2)) >= (rect->GetX() - (rect->GetW() / 2)) &&
                    (rect->GetY() + rect->GetH() / 2) >= (y - (w / 2)) &&
                    (y + (w / 2)) >= (rect->GetY() - (rect->GetH() / 2))))
                {
                    return false;
                }
            }
        }
        return true;
    }
};

void main()
{
    Rectangulo *rectanguloA = new Rectangulo(1.f, 1.f, 2.f, 2.f);
    Rectangulo *rectanguloB = new Rectangulo(3.f, 0.f, 2.f, 2.f);
    Rectangulo *rectanguloC = new Rectangulo(0.f, 3.f, 2.f, 2.f);
    std::vector<Rectangulo *> rectangulos;
    rectangulos.push_back(rectanguloA);
    rectangulos.push_back(rectanguloB);
    rectangulos.push_back(rectanguloC);
    std::cout << rectanguloA->CollisionEntreTodosLosTriangulos(rectangulos);
}