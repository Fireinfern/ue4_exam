# Pregunta 2
Si se desea obtener colisiones entre los rectangulos se podria utilizar la siguiente clase:
```c++
class Rectangulo
{
private:
    float x;
    float y;
    float w;
    float h;

public:
    Rectangulo() {}
    Rectangulo(float _x, float _y, float _w, float _h)
    {
        x = _x;
        y = _y;
        w = _w;
        h = _h;
    }
    ~Rectangulo() {}
    float GetX() { return x; }
    float GetY() { return y; }
    float GetW() { return w; }
    float GetH() { return h; }
    bool CollisionEntreTodosLosTriangulos(std::vector<Rectangulo *> &rectangulos)
    {
        bool ThisRect = false;
        for (auto rect : rectangulos)
        {
            if (!ThisRect && rect->GetX() == x && rect->GetY() == y)
                ThisRect = true;
            else
            {
                if ((rect->GetX() + (rect->GetW() / 2)) >= (x - (w / 2)) &&
                    (x + (w / 2)) >= (rect->GetX() - (rect->GetW() / 2)) &&
                    (rect->GetY() + rect->GetH() / 2) >= (y - (w / 2)) &&
                    (y + (w / 2)) >= (rect->GetY() - (rect->GetH() / 2)))
                {
                    return true;
                }
            }
        }
        return false;
    }
};
```
Donde la fucnionCollision entre todos los triangulos retornaria verdadero si existe una colision con uno de los rectangulos. Para hacer que este pueda detectar si existe colision entre los tres al mismo tiempo se tendria que hacer unos pequeños cambios, tal que:
```c++
bool CollisionEntreTodosLosTriangulos(std::vector<Rectangulo *> &rectangulos)
    {
        bool ThisRect = false;
        for (auto rect : rectangulos)
        {
            if (!ThisRect && rect->GetX() == x && rect->GetY() == y)
                ThisRect = true;
            else
            {
                if (!((rect->GetX() + (rect->GetW() / 2)) >= (x - (w / 2)) &&
                    (x + (w / 2)) >= (rect->GetX() - (rect->GetW() / 2)) &&
                    (rect->GetY() + rect->GetH() / 2) >= (y - (w / 2)) &&
                    (y + (w / 2)) >= (rect->GetY() - (rect->GetH() / 2))))
                {
                    return false;
                }
            }
        }
        return true;
    }
```
De esta manera, la funcion retornaria falso si no esta colisionando con uno de los rectangulos, y retornaria verdadero si el rectangulo que invoca esta colisionando con los otros dos rectangulos.
El codigo se encuentra en el archivo pregunta2.cpp

[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)