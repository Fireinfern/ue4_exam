# Pregunta 6
Se tienen las siguientes caracteristicas:
- Lista de enemigos llamada **activeEnemies**
- tiempo **t**
- **N** es el numero maximo de ataques encadenados en un solo Interlink
- una funcion **OnMeleeAttackConnected()** cuando un jugador conecta un golpe a un enemigo.
- una funcion **DoDamage(entero amount)** que reduce la vida del enemigo en amount

```
function RepetirDanioInterlink(arreglo enemigos,entero n)
do
    declarar rand como entero
    asignar rand a generadorDeNumerosAleatorios(1, 100)
    if rand <= 10
    do
        for
        do
            enem = activeEnemies[generadorDeNumerosAleatorios(0, len(activeEnemies))]
            if enem no esta en enemigos
            do
                n++
                if n < N
                do
                    enem call DoDamage(10)
                    enemigos add enem
                    RepetirDanioInterlink(enemigos, n)
                    break;
                end
            end
        end
    end
end

function OnMeleeAttackConnected()
do
    declarar rand como entero
    asignar rand a generadorDeNumerosAleatorios(1, 100)
    declarar n como 0
    if rand <= 25
    declarar enemigos como []
    do
        enemigo call DoDamage(10)
        enemigos add enemigo
        RepetirDanioInterlink(enemigos, n)
    end
end
```
Lo unico que faltaria en este pseudocodigo es la forma de verificar que tan sercano se encuentra, esto seria mas sencillo si se utilizara el enemigo mas cercano en vez de uno aleatorio entre todos dentro de un rango. Con UE podemos crear un triggervolume con la forma de un cilindro o caja que nos ayude a identificar que actores se encuentran en rango de el siguiente rebote. Esto haria posible el no utilizar una lista de enemigos activos en escena. ya que dengro del trigger volum se puede obtener la lista de actores y de ahi procederia elejir uno aleatoreo si se activa el salto de  InterLink. Este trigger volume se tendria que mover a la posicion del ultimo enemigo que daño el rebote de Interlink y consevar una lista de enemigos que ya fueron afectados por este.

[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)