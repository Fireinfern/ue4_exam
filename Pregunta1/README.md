# Pregunta 1
Este codigo nos devuelve si el numero n es un numero primo mayor a 2, tomando en cuenta que todo numero natural puede ser producto de combinaciones de numeros primos, y la raiz cuadrada del numero aproximada hacia el numero mayor, seria el numero primo mayor que puede ser parte de la opeacion para optener n. En el caso de este algoritmo utilizan todos los numeros mayores a dos hasta este "primo" aproximado por cuestinoes practicas.
```c++
bool xyz (int n) {
    float i = math.sqrt(n);
    int j = math.ceil(i);
    int k = 2;
    int x = k;
    while(x <= j) {
        if(!(n % x))
            return false;
        else
            x++;
    }
    return true;
}
```
[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)