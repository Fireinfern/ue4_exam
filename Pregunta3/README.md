# Pregunta 3
Para el desarrollo de la pregunta 3 se utilizo Windows forms y el componente Graphics, el cual permite graficar en alguna de las herramientas de WF, en el caso de mi aplicacion, se dibuja en un pirctureBox. Dentro de este se puede encontrar el siguiente algoritmo:
```c++
int X = (int)this->numX->Value;
int Y = (int)this->numY->Value;
int Radio = (int)this->numRadio->Value;
int N = (int)this->numN->Value;
float angulo = 0.f;
float incremento = 180.f / (float)N;
for (int i = 0; i < N; i++)
{
	Rectangle rec(X - 2 + cos(angulo*PI/180) * Radio, Y - 2 + sin(angulo*PI/180) * Radio, 4, 4);
	graficador->DrawIcon(SystemIcons::Asterisk, rec);
    angulo += incremento;
}
```
Sabiendo que se definio Pi como una macro con el valor 3.14159265, y se utilizo la libreria cmath para el calculo de seno y coseno, con lo cual se hallaron los puntos en donde se dibujara el asterisco en este caso. El ejecutable se encuentra en la carpeta Pregunta3

[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)