#pragma once
#include <math.h>

#define PI 3.14159265

namespace CppCLRWinformsProjekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private:
		Graphics^ graficador;
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
			graficador = this->pbxDrawing->CreateGraphics();
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			delete graficador;
		}
	private: System::Windows::Forms::Label^ lblRadio;
	private: System::Windows::Forms::NumericUpDown^ numRadio;
	private: System::Windows::Forms::Label^ lblN;
	private: System::Windows::Forms::NumericUpDown^ numN;
	private: System::Windows::Forms::NumericUpDown^ numY;
	private: System::Windows::Forms::Label^ lblY;
	private: System::Windows::Forms::NumericUpDown^ numX;
	private: System::Windows::Forms::Label^ lblX;
	private: System::Windows::Forms::PictureBox^ pbxDrawing;
	private: System::Windows::Forms::Button^ btnDibujar;

	protected:

	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lblRadio = (gcnew System::Windows::Forms::Label());
			this->numRadio = (gcnew System::Windows::Forms::NumericUpDown());
			this->lblN = (gcnew System::Windows::Forms::Label());
			this->numN = (gcnew System::Windows::Forms::NumericUpDown());
			this->numY = (gcnew System::Windows::Forms::NumericUpDown());
			this->lblY = (gcnew System::Windows::Forms::Label());
			this->numX = (gcnew System::Windows::Forms::NumericUpDown());
			this->lblX = (gcnew System::Windows::Forms::Label());
			this->pbxDrawing = (gcnew System::Windows::Forms::PictureBox());
			this->btnDibujar = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numRadio))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numN))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numY))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numX))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxDrawing))->BeginInit();
			this->SuspendLayout();
			// 
			// lblRadio
			// 
			this->lblRadio->AutoSize = true;
			this->lblRadio->Location = System::Drawing::Point(13, 13);
			this->lblRadio->Name = L"lblRadio";
			this->lblRadio->Size = System::Drawing::Size(35, 13);
			this->lblRadio->TabIndex = 0;
			this->lblRadio->Text = L"Radio";
			this->lblRadio->Click += gcnew System::EventHandler(this, &Form1::label1_Click);
			// 
			// numRadio
			// 
			this->numRadio->DecimalPlaces = 8;
			this->numRadio->Location = System::Drawing::Point(54, 11);
			this->numRadio->Name = L"numRadio";
			this->numRadio->Size = System::Drawing::Size(120, 20);
			this->numRadio->TabIndex = 2;
			// 
			// lblN
			// 
			this->lblN->AutoSize = true;
			this->lblN->Location = System::Drawing::Point(207, 13);
			this->lblN->Name = L"lblN";
			this->lblN->Size = System::Drawing::Size(15, 13);
			this->lblN->TabIndex = 3;
			this->lblN->Text = L"N";
			// 
			// numN
			// 
			this->numN->Location = System::Drawing::Point(229, 10);
			this->numN->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100000, 0, 0, 0 });
			this->numN->Name = L"numN";
			this->numN->Size = System::Drawing::Size(120, 20);
			this->numN->TabIndex = 4;
			// 
			// numY
			// 
			this->numY->Location = System::Drawing::Point(229, 36);
			this->numY->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100000, 0, 0, 0 });
			this->numY->Name = L"numY";
			this->numY->Size = System::Drawing::Size(120, 20);
			this->numY->TabIndex = 8;
			// 
			// lblY
			// 
			this->lblY->AutoSize = true;
			this->lblY->Location = System::Drawing::Point(207, 39);
			this->lblY->Name = L"lblY";
			this->lblY->Size = System::Drawing::Size(14, 13);
			this->lblY->TabIndex = 7;
			this->lblY->Text = L"Y";
			// 
			// numX
			// 
			this->numX->Location = System::Drawing::Point(54, 37);
			this->numX->Name = L"numX";
			this->numX->Size = System::Drawing::Size(120, 20);
			this->numX->TabIndex = 6;
			// 
			// lblX
			// 
			this->lblX->AutoSize = true;
			this->lblX->Location = System::Drawing::Point(13, 39);
			this->lblX->Name = L"lblX";
			this->lblX->Size = System::Drawing::Size(14, 13);
			this->lblX->TabIndex = 5;
			this->lblX->Text = L"X";
			// 
			// pbxDrawing
			// 
			this->pbxDrawing->Location = System::Drawing::Point(16, 82);
			this->pbxDrawing->Name = L"pbxDrawing";
			this->pbxDrawing->Size = System::Drawing::Size(358, 167);
			this->pbxDrawing->TabIndex = 9;
			this->pbxDrawing->TabStop = false;
			// 
			// btnDibujar
			// 
			this->btnDibujar->Location = System::Drawing::Point(274, 62);
			this->btnDibujar->Name = L"btnDibujar";
			this->btnDibujar->Size = System::Drawing::Size(75, 23);
			this->btnDibujar->TabIndex = 10;
			this->btnDibujar->Text = L"Dibujar";
			this->btnDibujar->UseVisualStyleBackColor = true;
			this->btnDibujar->Click += gcnew System::EventHandler(this, &Form1::btnDibujar_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(386, 261);
			this->Controls->Add(this->btnDibujar);
			this->Controls->Add(this->pbxDrawing);
			this->Controls->Add(this->numY);
			this->Controls->Add(this->lblY);
			this->Controls->Add(this->numX);
			this->Controls->Add(this->lblX);
			this->Controls->Add(this->numN);
			this->Controls->Add(this->lblN);
			this->Controls->Add(this->numRadio);
			this->Controls->Add(this->lblRadio);
			this->Name = L"Form1";
			this->Text = L"Form1";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numRadio))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numN))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numY))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numX))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxDrawing))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void btnDibujar_Click(System::Object^ sender, System::EventArgs^ e) {
		int X = (int)this->numX->Value;
		int Y = (int)this->numY->Value;
		int Radio = (int)this->numRadio->Value;
		int N = (int)this->numN->Value;
		float angulo = 0.f;
		float incremento = 180.f / (float)N;
		for (int i = 0; i < N; i++)
		{
			Rectangle rec(X - 2 + cos(angulo*PI/180) * Radio, Y - 2 + sin(angulo*PI/180) * Radio, 4, 4);
			graficador->DrawIcon(SystemIcons::Asterisk, rec);
			angulo += incremento;
		}
	}
};
}
