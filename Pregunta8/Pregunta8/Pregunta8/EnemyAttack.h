#pragma once
#include <string>
#include <vector>

class EnemyAttack
{
private:
	float attackDuration;
	std::vector<std::string> attackTags;
	std::vector<std::string> reactionTags;
public:
	EnemyAttack();
	EnemyAttack(float _attackDuration);
	~EnemyAttack();
	float GetAttackDuration() { return attackDuration; }
	void SetAttackDuration(float _attackDuration) { attackDuration = _attackDuration; }
	std::vector<std::string> GetAttackTags() { return attackTags; };
	void addAttackTag(std::string tag);
	std::vector<std::string> GetReactionTags() { return reactionTags; }
	void addReactionTag(std::string tag);
};

