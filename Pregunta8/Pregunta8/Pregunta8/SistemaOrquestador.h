#pragma once
#include "Enemy.h"
#include <chrono>
#include <iostream>

class SistemaOrquestador
{
private:
	std::vector<Enemy*> activeEnemies;
	std::chrono::steady_clock::time_point beginAttack;
	float T;
	int N;
	bool isAttacking;
	bool readyForAttack;

	float R = 25.f;
	bool NVerification = true;
public:
	SistemaOrquestador();
	~SistemaOrquestador();
	std::vector<Enemy*> GetActiveEnemies() { return activeEnemies; }
	void addActiveEnemy(Enemy* enemigo) { activeEnemies.push_back(enemigo); }
	bool AtackCooldown();
	void initAttack( Enemy* enemigo);
	bool GetReadyForAttack() { return readyForAttack; }
	void incrementNormalAttackCounter() { 
		N++; 
	}
};

