#include "Game.h"
#include <iostream>
#include <conio.h>

Game::Game()
{
	gameState = GameState::PLAY;
	SistemaOrquestador* director;
}

Game::~Game()
{
}

void Game::GameInit()
{
	this->director = new SistemaOrquestador();
	// Instancia de Rana
	Rana = new Enemy("Rana", "Terrestre");
	EnemyAttack* RanaAtaque1 = new EnemyAttack(5.0f); // Se establece la creacion del ataque y su duracion
	RanaAtaque1->addAttackTag("flincher");
	RanaAtaque1->addAttackTag("knockdown");
	RanaAtaque1->addReactionTag("bullet");
	Rana->AddEnemyAttack(RanaAtaque1);

	// Instancia de Tronco
	Tronco = new Enemy("Tronco", "Terrestre");
	EnemyAttack* TroncoAtaque1 = new EnemyAttack(8.0f);
	TroncoAtaque1->addAttackTag("charger");
	TroncoAtaque1->addAttackTag("distance");
	TroncoAtaque1->addReactionTag("knockdown");
	Tronco->AddEnemyAttack(TroncoAtaque1);
	
	// Instancia de Mono
	Mono = new Enemy("Mono", "Aereo");
	EnemyAttack* MonoAtaque1 = new EnemyAttack(2.f);
	MonoAtaque1->addAttackTag("distance");
	MonoAtaque1->addAttackTag("bullet");
	MonoAtaque1->addReactionTag("charger");
	Mono->AddEnemyAttack(MonoAtaque1);

	// Iniciar enemigos activos
	this->director->addActiveEnemy(Rana);
	this->director->addActiveEnemy(Tronco);
	this->director->addActiveEnemy(Mono);
}

void Game::runGame()
{
	int c = 0;
	while (gameState == PLAY) {
		c = 0;
		director->AtackCooldown();
		if (_kbhit()) {
			switch (c = _getch())
			{
			case 27:
				gameState = EXIT;
				break;
			case 113:
				std::cout << "El enemigo " << Rana->GetEnemyName() << " esta intentnado atacar" << std::endl;
				Rana->Attack(director);
				break;
			case 119:
				std::cout << "El enemigo " << Tronco->GetEnemyName() << " esta intenando atacar" << std::endl;
				Tronco->Attack(director);
				break;
			case 116:
				std::cout << "El enemigo " << Mono->GetEnemyName() << " esta intenando atacar" << std::endl;
				Mono->Attack(director);
				break;
			}
		}
		for (auto enemy : director->GetActiveEnemies()) {
			enemy->update();
		}
		
	}
	std::cout << "Hasta Pronto" << std::endl;
	this->~Game();
}
