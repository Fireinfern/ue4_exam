#pragma once
#include "SistemaOrquestador.h"
#include "Enemy.h"

enum GameState {
	PLAY,
	EXIT
};

class Game
{
private:
	GameState gameState;
	SistemaOrquestador* director;
	// Enemigos de Prueba
	Enemy* Rana;
	Enemy* Tronco;
	Enemy* Mono;
public:
	Game();
	~Game();
	void GameInit();
	void runGame();
};

