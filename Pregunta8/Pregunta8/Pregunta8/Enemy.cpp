#include "Enemy.h"
#include <iostream>
#include "SistemaOrquestador.h"
#include <chrono>

Enemy::Enemy()
{
}

Enemy::Enemy(std::string _nombre, std::string _enemyType)
{
	nombre = _nombre;
	enemyType = _enemyType;
	//enemyAttack = new EnemyAttack(2.f);
	// Testing
	/*
	enemyAttack->addAttackTag("basico");
	enemyAttack->addReactionTag("Atras");
	*/
	isAttacking = false;
}

Enemy::~Enemy()
{
}

void Enemy::Attack(SistemaOrquestador* director)
{
	if (isAttacking) {
		std::cout << "El enemigo " << nombre << " no puede atacar porque ya esta atacando" << std::endl;
		return;
	}
	for (auto enemy : director->GetActiveEnemies()) {
		if (enemy->GetEnemyName() != nombre) {
			if (enemy->GetisAttacking() && enemy->GetEnemyType() == enemyType) {
				std::cout << "El ataque de " << nombre << " no se puede llevar a cabo porque otro enemigo del tipo " << enemyType << " ya esta atacando" << std::endl;
				return;
			}
		}
	}
	isAttacking = true;
	beginAttack = std::chrono::steady_clock::now();
	director->incrementNormalAttackCounter();
	director->initAttack(this);
}

void Enemy::AttackOrquestado()
{
	std::cout << "El enemigo " << nombre << " esta en un ataque orquestado" << std::endl;
	if (!isAttacking){
		isAttacking = true;
		beginAttack = std::chrono::steady_clock::now();
	}
}

void Enemy::update()
{
	if (isAttacking) {
		if (enemyAttacks[0]->GetAttackDuration() < std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - beginAttack).count()) {
			isAttacking = false;
			std::cout << "La ventana de bloquedo de ataque generada por " << nombre << " ha terminado" << std::endl;
		}
	}
}
