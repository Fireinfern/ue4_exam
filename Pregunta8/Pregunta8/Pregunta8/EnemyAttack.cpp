#include "EnemyAttack.h"

EnemyAttack::EnemyAttack()
{
}

EnemyAttack::EnemyAttack(float _attackDuration)
{
	attackDuration = _attackDuration;
}

EnemyAttack::~EnemyAttack()
{
}

void EnemyAttack::addAttackTag(std::string tag)
{
	attackTags.push_back(tag);
}

void EnemyAttack::addReactionTag(std::string tag)
{
	reactionTags.push_back(tag);
}
