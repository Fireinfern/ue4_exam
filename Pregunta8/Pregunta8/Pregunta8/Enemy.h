#pragma once
#include <stdio.h>
#include <string>
#include "EnemyAttack.h"
#include <chrono>

class SistemaOrquestador;

class Enemy
{
private:
	std::string nombre;
	// Podria ser un struct de EnemyType
	std::string enemyType;
	std::vector<EnemyAttack*> enemyAttacks;
	bool ataqueOrquestado;
	std::chrono::steady_clock::time_point beginAttack;
	bool isAttacking;
public:
	Enemy();
	Enemy(std::string _nombre, std::string _enemyType);
	~Enemy();
	std::string GetEnemyName() { return nombre; }
	std::string GetEnemyType() { return enemyType; }
	void SetEnemyType(std::string _enemyType) { enemyType = _enemyType; }
	std::vector<EnemyAttack*> GetEnemyAttacks() { return enemyAttacks; }
	void AddEnemyAttack(EnemyAttack* attack) { enemyAttacks.push_back(attack); }
	void Attack(SistemaOrquestador* director);
	void AttackOrquestado();
	void SetAtaqueOrquestado(bool val) { ataqueOrquestado = val; }
	bool GetAtaqueOrquestado() { return ataqueOrquestado; }
	bool GetisAttacking() { return isAttacking; }
	void update();
};

