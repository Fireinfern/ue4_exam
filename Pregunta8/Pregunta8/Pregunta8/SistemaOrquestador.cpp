#include "SistemaOrquestador.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

SistemaOrquestador::SistemaOrquestador()
{
	isAttacking = false;
	readyForAttack = true;
	T = 20;
	N = 8;
}

SistemaOrquestador::~SistemaOrquestador()
{
}

bool SistemaOrquestador::AtackCooldown()
{
	if (isAttacking) {
		if (T < std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - beginAttack).count()) {
			isAttacking = false;
			std::cout << "El bloqueo de ataques orquestados ha terminado" << std::endl;
		}
	}
	if (!NVerification && N >= 8) {
		std::cout << "Se alcanzo la cantidad de ataques normales(8) para activar los ataques Orquestados" << std::endl;
		NVerification = true;
	}
	return false;
}

void SistemaOrquestador::initAttack(Enemy* _enemigo)
{	
	if (!isAttacking && N >= 8) {
		srand(time(NULL));
		if (T >= (rand() % 101)) {
			beginAttack = std::chrono::steady_clock::now();
			isAttacking = true;
			N = 0;
			NVerification = false;
			for (auto enemigo: activeEnemies){
				if (enemigo->GetEnemyName() != _enemigo->GetEnemyName()) {
					if (!enemigo->GetisAttacking()) {
						for (auto i: _enemigo->GetEnemyAttacks()){
							for (auto j: enemigo->GetEnemyAttacks()){
								for (auto aTag : i->GetAttackTags()) {
									// j->GetReactionTags();
									for (auto rTag : j->GetReactionTags()) {
										if (aTag == rTag) {
											enemigo->AttackOrquestado();
											_enemigo->AttackOrquestado();
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
