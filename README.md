# Examen de conocimientos de UnrealEngine

- Examen de Conocimiento de UnrealEngine
  - [Pregunta 1](./Pregunta1/)
  - [Pregunta 2](./Pregunta2/)
  - [Pregunta 3](./Pregunta3/)
  - [Pregunta 4](./Pregunta4/)
  - [Pregunta 5](./Pregunta5/)
  - [Pregunta 6](./Pregunta6/)
  - [Pregunta 7](./Pregunta7/)
  - [Pregunta 8](./Pregunta8/)

<!-- ## Pregunta 1
Este codigo nos devuelve si el numero n es un numero primo mayor a 2, tomando en cuenta que todo numero natural puede ser producto de combinaciones de numeros primos, y la raiz cuadrada del numero aproximada hacia el numero mayor, seria el numero primo mayor que puede ser parte de la opeacion para optener n. En el caso de este algoritmo utilizan todos los numeros mayores a dos hasta este "primo" aproximado por cuestinoes practicas.
```c++
bool xyz (int n) {
    float i = math.sqrt(n);
    int j = math.ceil(i);
    int k = 2;
    int x = k;
    while(x <= j) {
        if(!(n % x))
            return false;
        else
            x++;
    }
    return true;
}
```
[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)
## Pregunta 2
Si se desea obtener colisiones entre los rectangulos se podria utilizar la siguiente clase:
```c++
class Rectangulo
{
private:
    float x;
    float y;
    float w;
    float h;

public:
    Rectangulo() {}
    Rectangulo(float _x, float _y, float _w, float _h)
    {
        x = _x;
        y = _y;
        w = _w;
        h = _h;
    }
    ~Rectangulo() {}
    float GetX() { return x; }
    float GetY() { return y; }
    float GetW() { return w; }
    float GetH() { return h; }
    bool CollisionEntreTodosLosTriangulos(std::vector<Rectangulo *> &rectangulos)
    {
        bool ThisRect = false;
        for (auto rect : rectangulos)
        {
            if (!ThisRect && rect->GetX() == x && rect->GetY() == y)
                ThisRect = true;
            else
            {
                if ((rect->GetX() + (rect->GetW() / 2)) >= (x - (w / 2)) &&
                    (x + (w / 2)) >= (rect->GetX() - (rect->GetW() / 2)) &&
                    (rect->GetY() + rect->GetH() / 2) >= (y - (w / 2)) &&
                    (y + (w / 2)) >= (rect->GetY() - (rect->GetH() / 2)))
                {
                    return true;
                }
            }
        }
        return false;
    }
};
```
Donde la fucnionCollision entre todos los triangulos retornaria verdadero si existe una colision con uno de los rectangulos. Para hacer que este pueda detectar si existe colision entre los tres al mismo tiempo se tendria que hacer unos pequeños cambios, tal que:
```c++
bool CollisionEntreTodosLosTriangulos(std::vector<Rectangulo *> &rectangulos)
    {
        bool ThisRect = false;
        for (auto rect : rectangulos)
        {
            if (!ThisRect && rect->GetX() == x && rect->GetY() == y)
                ThisRect = true;
            else
            {
                if (!((rect->GetX() + (rect->GetW() / 2)) >= (x - (w / 2)) &&
                    (x + (w / 2)) >= (rect->GetX() - (rect->GetW() / 2)) &&
                    (rect->GetY() + rect->GetH() / 2) >= (y - (w / 2)) &&
                    (y + (w / 2)) >= (rect->GetY() - (rect->GetH() / 2))))
                {
                    return false;
                }
            }
        }
        return true;
    }
```
De esta manera, la funcion retornaria falso si no esta colisionando con uno de los rectangulos, y retornaria verdadero si el rectangulo que invoca esta colisionando con los otros dos rectangulos.
El codigo se encuentra en el archivo pregunta2.cpp

[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)
## Pregunta 3
Para el desarrollo de la pregunta 3 se utilizo Windows forms y el componente Graphics, el cual permite graficar en alguna de las herramientas de WF, en el caso de mi aplicacion, se dibuja en un pirctureBox. Dentro de este se puede encontrar el siguiente algoritmo:
```c++
int X = (int)this->numX->Value;
int Y = (int)this->numY->Value;
int Radio = (int)this->numRadio->Value;
int N = (int)this->numN->Value;
float angulo = 0.f;
float incremento = 180.f / (float)N;
for (int i = 0; i < N; i++)
{
	Rectangle rec(X - 2 + cos(angulo*PI/180) * Radio, Y - 2 + sin(angulo*PI/180) * Radio, 4, 4);
	graficador->DrawIcon(SystemIcons::Asterisk, rec);
    angulo += incremento;
}
```
Sabiendo que se definio Pi como una macro con el valor 3.14159265, y se utilizo la libreria cmath para el calculo de seno y coseno, con lo cual se hallaron los puntos en donde se dibujara el asterisco en este caso. El ejecutable se encuentra en la carpeta Pregunta3

[Proyecto de Pregunta 3](https://gitlab.com/Fireinfern/pregunta3_ue4_leap)

[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)
## Pregunta 4
[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)
## Pregunta 5
```c++
// vector de posiciones del jugador
player->pos(x,y,z);

// vector de direccion del jugador (direccion donde el jugador este mirando actualmente)
player->pForward(x,y,z);

// lista de enemigos activa
vector<*Enemigo> activeEnemies;

// debe existir una clase Player que sea un UActor
class Player{}

Enemigo Player::LockOnEnemie(vector<*Enemigo> &_activeEnemies){
    // este valor vendria a ser la posicion del jugador que se nombro al inicio player->pos(x,y,z)
    FVector pos = GetOwner()->GetActorLocation();

    // valor que ayudara a almacenar el enemigo a retornar, al iniciar sera nulo por si no hay enemigos que utilizar en el lockon
    Enemigo enemigoRespuesta = null;

    // Valor de distancia inicial
    float distanciaMenor = 9000000.f

    for (auto enemie:_activeEnemies){
        // evitamos verificar a los enemigos que no hayan sido renderizados hace un segundo
        if(enemie->WasRecentlyRendered(1.f)){
            // esta funcion dara la distancia entre la posicion del jugador y la posicion del enemigo actualmente revisado
            float distancia = FVector::Dist(pos, enemie->GetActorLocation());
            // validamos que no haya mas de 20u de distancia entre personaje y enemigo
            if (distancia <= 20.f){
                if (distancia < distanciaMenor){
                    float angle = (enemie->GetActorLocation() - player->GetActorLocation()).ToOrientationRotator().Yaw - player->GetPFoward().ToOrientationRotator().Yaw;
                    if (angle < x && angle > -x){
                        distanciaMenor = distancia;
                        enemigoRespuesta = enemie;
                    }
                }
            }
        }
    }
    return enemigoRespuesta;
}
```
Asumiendo que hay funciones para tener el vector de pFoward llamada GetPFoward, se puede recorrer el arreglo de enemigos activos, verificando que el enemigo se haya renderizado en el ultimo segundo, para evitar asi entrar a revisar enemigos que no estan siendo renderizados. Luego de eso se debe verificar si la distancia es mayor a las 20u establecidas, para ello se utilizo la funcion Dist de FVector, esta puede ser cambiada por FVector::Dist2D si se cree que la coordenada z es inecesaria para este calculo de la unidad. Para no escoger simplemente al primer enemigo se utiliza un valor de distancia menor, la cual ayudara a elegir el enemigo mas cercano que se encuentre dentro de los angulos establecidos de preferencia llamado x. para ello se utiliza en yaw de la orientacion del actor para saber a que angulo se encutran de la vision del personaje.

[Retornar Al Inicio](https://gitlab.com/Fireinfern/ue4_exam/-/tree/master)
## Pregunta 6
Se tienen las siguientes caracteristicas:
- Lista de enemigos llamada **activeEnemies**
- tiempo **t**
- **N** es el numero maximo de ataques encadenados en un solo Interlink
- una funcion **OnMeleeAttackConnected()** cuando un jugador conecta un golpe a un enemigo.
- una funcion **DoDamage(entero amount)** que reduce la vida del enemigo en amount

```
function RepetirDanioInterlink(arreglo enemigos,entero n)
do
    declarar rand como entero
    asignar rand a generadorDeNumerosAleatorios(1, 100)
    if rand <= 10
    do
        for
        do
            enem = activeEnemies[generadorDeNumerosAleatorios(0, len(activeEnemies))]
            if enem no esta en enemigos
            do
                n++
                if n < N
                do
                    enem call DoDamage(10)
                    enemigos add enem
                    RepetirDanioInterlink(enemigos, n)
                    break;
                end
            end
        end
    end
end

function OnMeleeAttackConnected()
do
    declarar rand como entero
    asignar rand a generadorDeNumerosAleatorios(1, 100)
    declarar n como 0
    if rand <= 25
    declarar enemigos como []
    do
        enemigo call DoDamage(10)
        enemigos add enemigo
        RepetirDanioInterlink(enemigos, n)
    end
end
```
Lo unico que faltaria en este pseudocodigo es la forma de verificar que tan sercano se encuentra, esto seria mas sencillo si se utilizara el enemigo mas cercano en vez de uno aleatorio entre todos dentro de un rango. Con UE podemos crear un triggervolume con la forma de un cilindro o caja que nos ayude a identificar que actores se encuentran en rango de el siguiente rebote. Esto haria posible el no utilizar una lista de enemigos activos en escena. ya que dengro del trigger volum se puede obtener la lista de actores y de ahi procederia elejir uno aleatoreo si se activa el salto de  InterLink. Este trigger volume se tendria que mover a la posicion del ultimo enemigo que daño el rebote de Interlink y consevar una lista de enemigos que ya fueron afectados por este.

[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)

## Pregunta 7
La solucion puede depender de los parametros que se tienen, por ejemplo, una solucion valida si el numero de enemigos en pantalla nunca va a ser mayor a n, se pueden instanciar n enemigos al inicio del nivel y resetear los valores iniciales de estos cada vez que uno sale de pantalla o es "eliminado" por el jugador, de tal manera que nunca se instancian enemigos de mas y solo se borran de memoria al terminar el nivel. En el caso de que uno de estos no necesite estar en uso, solo se puede dejar de renderizar para no utilizar recursos de mas.

[Retornar Al Inicio](#examen-de-conocimientos-de-unrealengine)

## Pregunta 8 -->
